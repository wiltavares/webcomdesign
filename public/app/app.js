'use strict';
﻿var app = angular.module('app', ['ui.router', 'firebase']);

/*app.run(function ($rootScope, $timeout) {
    $rootScope.$on('$viewContentLoaded', function () {
        $timeout(function () {
            componentHandler.upgradeAllRegistered();
        });
    });
});*/

app.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider

        .state('home', {
            url: '/home',
            templateUrl: '/app/views/home.html',
            controller: 'HomeCtrl'
        })
    
        .state('servicos', {
            url: '/servicos',
            templateUrl: '/app/views/servicos.html',
            controller: 'ServicosCtrl'
        })
    
        .state('empresa', {
            url: '/empresa',
            templateUrl: '/app/views/empresa.html',
            controller: 'EmpresaCtrl'
        })
    
        .state('portifolio', {
            url: '/portifolio',
            templateUrl: '/app/views/portifolio.html',
            controller: 'PortifolioCtrl'
        })
    
        .state('contato', {
            url: '/contato',
            templateUrl: '/app/views/contato.html',
            controller: 'ContatoCtrl'
        })
  
    $urlRouterProvider.otherwise('/home');

});

app.directive('menuClose', function () {
    return {
        restrict: 'AC',
        link: function ($scope, $element) {
            $element.bind('click', function () {
                debugger;
                var d = document.querySelector('.side-nav');
                d.MaterialLayout.toggleDrawer();
            });
        }
    };
});

/*app.config(["$locationProvider", function ($locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
}]);*/
