app.factory('dbFactorie', function($q){
    
    var config = {
        apiKey: "AIzaSyDEIJ7HXr9CMQoCndBfaOc7GGxhSxHBxGU",
        authDomain: "webcomdesign-78df0.firebaseapp.com",
        databaseURL: "https://webcomdesign-78df0.firebaseio.com",
        storageBucket: "webcomdesign-78df0.appspot.com",
    };
    firebase.initializeApp(config);
   
    var service = {};
    var database = firebase.database();
    
    
    service.setContato = function(item){
        var deferred = $q.defer();
        
        database.ref().child('contatos').push(item).then(function (result) {
            deferred.resolve("Registro inserido!");
        }).catch(function (result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });
        return deferred.promise;
    }
        
    return service;
});