'use strict';
app.controller("HomeCtrl", ['$scope', '$rootScope', function ($scope, $rootScope, $state) {
    $('.parallax').parallax();
    $('.button-collapse').sideNav();
    $('.collapsible').collapsible();
    
    $scope.onTrabalhosRecentes = false;
    $scope.onCriacaoRecente = false;
    $scope.titulo = undefined;
    $scope.mensagem = undefined;
    
    
    $scope.mostrarTituloMensagem = function(item){
        
        if (item == 'sistemasPersonalizados')
        {
            $scope.titulo = 'Sistemas personalizados';
            $scope.mensagem = 'Sistema planejado e desenvolvido de acordo com sua necessidade, oferecendo maior usabilidade.';
        }
        else if (item == 'vitrinesVirtuais')
        {
            $scope.titulo = 'Vitrines virtuais';
            $scope.mensagem = 'Seu produto ou serviço visível na web, de forma segura e de fácil acesso o tempo todo.';
        }
        else if (item == 'designResponsivo')
        {
            $scope.titulo = 'Design Responsivo';
            $scope.mensagem = 'O layout adapta o conteúdo a diferentes tipos de dispositivos e telas, proporcinando uma visualização mais confortável.';
        }
            
    }

}]);
