'use strict';
app.controller("ContatoCtrl", ['$scope', '$rootScope', 'dbFactorie', function ($scope, $rootScope, dbFactorie, $state) {
    
    $scope.salvarContato = function(item){
        $scope.item = angular.copy(item);
        
        dbFactorie.setContato($scope.item).then(function (data) {

            swal("Contato Enviado", "Em breve retornaremos! Fique a vontade caso queira nos ligar ou nos contatar nas redes sociais.", "success");
            $scope.item = {};
        }, function (result) {
            swal("Erro", result, "error");
        });  
    }

}]);